#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 20:26:00 2020
by Pouye Yazdi
"""
import os
import sys
sys.path.append('Functions')
import numpy as np

# ===========================================================================
# ...... For animating CFS result on horizontal cross sections
# ===========================================================================
from variables import CFSfilepath
from cfsPlotters import cfs_anim_2Dkm_H,cfs_plot_color
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
from basepolygons import basemapPolysll


#CFSname='out-89-35-0.1YearSSE-smoothm2-5x5km'
CFSname='out-89-35-3.0YearINTERSSE-smoothm1-5x5km'
maximumStress=1
mu=0.4
rake=-81
#line=15109+29 # For SSE smoothm0
#line=13124+29 # For SSE smoothm1
#line=11586+29 # For SSE smoothm2
#line=17264+29 # For INTERSSE smoothm0p4 over 5x5km patches
#line=17261+29 # For INTERSSE smoothm0p8 over 5x5km patches
#line=17263+29 # For INTERSSE smoothm0p8-div2 over 5x5km patches
line=17260+29 # For INTERSSE smoothm1 over 5x5km patches
#line=16999+29 # For INTERSSE smoothm1p4 over 5x5km patches

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.3) 
ax.set(xlabel='Longitude', ylabel='Latitude')
Cmap,Bounds=cfs_plot_color(maximumStress)
#ax.plot(longlist,latlist,'-y')
#ax.plot(longlist_5,latlist_5,':y')
#ax.plot(longlist5,latlist5,':y')
ax.grid()
landpoly,lakepoly=basemapPolysll(-102,-99,16.5,18)
ax.plot([cor[0] for cor in landpoly[:-2]],[cor[1] for cor in landpoly[:-2]],'-k')
CFSfilespath=os.path.join(CFSfilepath,'89-35--81/out/Hor')
arguments=[mu,rake,line,ax,CFSfilespath,CFSname,Cmap,Bounds]  
norm = matplotlib.colors.BoundaryNorm(Bounds, Cmap.N) 
ax1= fig.add_axes([0.1, 0.12, 0.8, 0.02])
matplotlib.colorbar.ColorbarBase(ax1,cmap=Cmap,boundaries=Bounds,extend='both',norm=norm,
                                ticks=np.dot(1,Bounds),
                                orientation='horizontal') 
for tick in ax1.get_xticklabels():
        tick.set_fontsize(8)
        tick.set_family("Courier New")
ax1.set_title('bar')
plt.rcParams['animation.ffmpeg_path'] = '/Users/pouye/anaconda/bin/ffmpeg'
anim = animation.FuncAnimation(fig, cfs_anim_2Dkm_H,fargs=[arguments],frames=8, repeat=False)
anim.save('%s/%s-animation-mu%.1f-rake%.1f-H.mp4'%(CFSfilespath,CFSname,mu,rake))
