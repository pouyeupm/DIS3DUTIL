#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 20:29:38 2020
by Pouye Yazdi
"""
import os
import sys
sys.path.append('Functions')
import numpy as np

# ===========================================================================
# ...... For plotting CFS result on vertical cross sections
# ===========================================================================
from variables import CFSfilepath,Inputsfilepath
from cfsPlotters import cfs_plot_2Dkm_V
from auxiliaries import fault_crossSection
from auxiliaries import new_upperCenter_coords
from auxiliaries import interface3D_crossSection

CFSfilename='out-89-35-3.0YearINTERSSE-smoothm0p4-5x5km-Ver-1km.txt'
earthquakefile=np.nan
mu=0.4
rake=-90
linenumber=17264+29
max_Stress=1#bar

planeAzimuth=22
dist=1
upC_lon,upC_lat=-100.05,17.05
new_lon,new_lat=new_upperCenter_coords(upC_lon,upC_lat,dist,planeAzimuth+90)
profile=[new_lon,new_lat,-8,planeAzimuth]

fault=fault_crossSection(12,16,int(CFSfilename.split('-')[1]),int(CFSfilename.split('-')[2]),planeAzimuth)
subductionfile=os.path.join(Inputsfilepath,'interface3D.txt')
subductionLine=interface3D_crossSection(subductionfile,planeAzimuth,new_lat,new_lon,50,2)
subductionLine=[s/1000 for s in subductionLine]
CFSfilespath=os.path.join(CFSfilepath,'89-35--81/out/Ver')
cfs_plot_2Dkm_V(CFSfilename,CFSfilespath,mu,rake,linenumber,max_Stress,earthquakefile,profile,fault,subductionLine)
figfilename='%s_mu%.1f.png'%(CFSfilename[:-4],mu)