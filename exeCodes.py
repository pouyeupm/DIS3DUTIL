#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 15:24:50 2020
by Pouye Yazdi
"""
import os
import sys
sys.path.append('Functions')
import numpy as np


# ===========================================================================
# ...... For 3D interface file construction
# ===========================================================================
from variables import SSEfilepath,SSEfilename,Inputsfilepath
asciifile=open(os.path.join(SSEfilepath,SSEfilename),'r')
lines=asciifile.readlines()[:]
asciifile.close()
interface3Dfile=open(os.path.join(Inputsfilepath,'interface3D.txt'),'w')
for i in range(len(lines)):
    long=round(float(lines[i].split()[0]),3)
    lat=round(float(lines[i].split()[1]),3)
    depth=round(float(lines[i].split()[2]),3) #in km and is given positive
    interface3Dfile.writelines('%.3f\t%.3f\t%.3f\n'%(long,lat,depth))
interface3Dfile.close()


# ===========================================================================
# ...... For slowslip 3D patch construction
# ===========================================================================
from variables import SSEmeshType,SSErotatedmeshAngle,SSEsurf_slip_az
from auxiliaries import slipData
from patchMakers import dis3dpatchMaker  
from variables import SSEinc_meter,SSEpatch_surface_length,SSEname
from variables import SSEfilename,SSEfilepath
surf_slip_az=SSEsurf_slip_az
rotatedmeshAngle=SSErotatedmeshAngle

if SSEmeshType=='rotated':
    surf_slip_az=surf_slip_az+SSErotatedmeshAngle
elif SSEmeshType=='global':
    rotatedmeshAngle=np.nan
file=os.path.join(SSEfilepath,SSEfilename)
X,Y,Z,TSSE=slipData(file,SSEinc_meter,SSEmeshType,SSErotatedmeshAngle)
allnormV=dis3dpatchMaker(SSEpatch_surface_length,SSEinc_meter,X,Y,Z,TSSE,surf_slip_az,rotatedmeshAngle,SSEname)


# ===========================================================================
# ...... For INTER-slowslip 3D patch construction
# ===========================================================================
from variables import INTERssemeshType,INTERsserotatedmeshAngle,INTERssesurf_slip_az
from auxiliaries import slipData
from patchMakers import dis3dpatchMaker  
from variables import INTERsseinc_meter,INTERssepatch_surface_length,INTERssename
from variables import INTERssefilename,INTERssefilepath

surf_slip_az=INTERssesurf_slip_az
rotatedmeshAngle=INTERsserotatedmeshAngle

if INTERssemeshType=='rotated':
    surf_slip_az=surf_slip_az+INTERsserotatedmeshAngle
elif INTERssemeshType=='global':
    rotatedmeshAngle=np.nan
file=os.path.join(INTERssefilepath,INTERssefilename)
X,Y,Z,INTERsse=slipData(file,INTERsseinc_meter,INTERssemeshType,INTERsserotatedmeshAngle)
allnormV=dis3dpatchMaker(INTERssepatch_surface_length,INTERsseinc_meter,X,Y,Z,INTERsse,surf_slip_az,rotatedmeshAngle,INTERssename)


# ===========================================================================
# ...... For correction of index and slip percentage for dis3d source inputs
# ===========================================================================  
from patchMakers import dis3dpatchIndexCorrector    
#filename='dis3dsource_AnnualINTERsse_smoothm0p4_ONgrid5x5km_patchL5km_using-22RotMesh.txt'
filename='dis3dsource_TotalSSE_smoothm0_ONgrid5x5km_patchL5km_using-22RotMesh.txt'
percentage=10
dis3dpatchIndexCorrector(filename,os.path.abspath('outputs/rotated-mesh'),17264,percentage)


# ===========================================================================
# ...... For making a vertical profile for dis3d estimations
# ===========================================================================
from auxiliaries import new_upperCenter_coords
from calcGridsMaker import dis3dCalc_ProfileMaker_V
import pyproj
from variables import calcGridsfilepath
planeAzimuth=22 
upC_lon,upC_lat=-100.05,17.05
L,W=50,30 # km
dL,dW=2,2 # km
Strike1,Dip1=75,41
dist=0
new_lon,new_lat=new_upperCenter_coords(upC_lon,upC_lat,dist,planeAzimuth+90)
name1='%ikm'%dist #stands for the nickname of the upperCenter point   
xlist,ylist=dis3dCalc_ProfileMaker_V(calcGridsfilepath,planeAzimuth,new_lat,new_lon,L,dL,W,dW,name1,Strike1,Dip1)
longlist,latlist=[],[]
p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
for i in range(len(xlist)):
    long,lat=p(xlist[i]*1000,ylist[i]*1000,inverse=True)
    longlist.append(long)
    latlist.append(lat)


new_lon,new_lat=new_upperCenter_coords(upC_lon,upC_lat,-5,planeAzimuth+90)
xlist_5,ylist_5=dis3dCalc_ProfileMaker_V(calcGridsfilepath,planeAzimuth,new_lat,new_lon,L,dL,W,dW,name1,Strike1,Dip1)
new_lon,new_lat=new_upperCenter_coords(upC_lon,upC_lat,5,planeAzimuth+90)
xlist5,ylist5=dis3dCalc_ProfileMaker_V(calcGridsfilepath,planeAzimuth,new_lat,new_lon,L,dL,W,dW,name1,Strike1,Dip1)

longlist_5,latlist_5=[],[]
p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
for i in range(len(xlist_5)):
    long,lat=p(xlist_5[i]*1000,ylist_5[i]*1000,inverse=True)
    longlist_5.append(long)
    latlist_5.append(lat)
longlist5,latlist5=[],[]
p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
for i in range(len(xlist5)):
    long,lat=p(xlist5[i]*1000,ylist5[i]*1000,inverse=True)
    longlist5.append(long)
    latlist5.append(lat) 

    
# ===========================================================================
# ...... For making a horizontal profile for dis3d estimations
# ===========================================================================
from calcGridsMaker import dis3dCalc_ProfileMaker_H
from variables import calcGridsfilepath
inc_grid2=5 #km
DEPTH=5
name2='Z%ikm'%DEPTH #stands for the nickname of the horizontal layer
Strike2,Dip2=98,52
dis3dCalc_ProfileMaker_H(calcGridsfilepath,-102,-99,16,18.5,inc_grid2,inc_grid2,DEPTH,name2,Strike2,Dip2)


# ===========================================================================
# ...... For making a 3D geometry for dis3d estimations along subduction
# ===========================================================================
from calcGridsMaker import dis3dCalc_interface
from variables import Inputsfilepath
inc_grid3=10 #km
Strike3,Dip3=101,33
dis3dCalc_interface(Inputsfilepath,inc_grid3,inc_grid3,Strike3,Dip3)