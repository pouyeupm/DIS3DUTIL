#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 19:52:32 2020
by Pouye Yazdi
"""

import os

Inputsfilepath=os.path.abspath('inputs')
Outputsfilepath=os.path.join(os.getenv("HOME"),'Google Drive/WORKING/COYUCA/dis3d-utility/outputs')
calcGridsfilepath=os.path.join(Outputsfilepath,'global-mesh')


SSEfilepath=os.path.join(Inputsfilepath,'alternative_SSE')
SSEinc_meter=5000 #in meter 
SSEpatch_surface_length=5000 #in meter 
SSEmeshType='rotated'
SSEfilename='slip_SSE_2002_etm0.txt'
SSEname='TotalSSE_smoothm0'
SSEsurf_slip_az=-158
SSErotatedmeshAngle=-22



INTERssefilepath=os.path.join(Inputsfilepath,'alternative_inter_SSE')
INTERsseinc_meter=15000 #in meter 
INTERssepatch_surface_length=15000 #in meter 
INTERssemeshType='global'
INTERssefilename='slip_inter_SSE_etm_m0p8.txt'
INTERssename='AnnualINTERsse_smoothm0p8'
INTERssesurf_slip_az=31
INTERsserotatedmeshAngle=-22

calcGridmeshType='global'
CFSfilepath=os.path.join(Outputsfilepath,'CFS-calculations')



