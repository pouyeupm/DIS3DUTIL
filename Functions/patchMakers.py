#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 09:39:19 2020
by Pouye Yazdi
"""

import os
import pandas as pd
import datetime
import numpy as np
import math
from sympy import Plane, Point3D
import operator
#*****************************************************************************
#*****************************************************************************
def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy
#*****************************************************************************
#*****************************************************************************
def plane3D_normal(vertex):
    P1=vertex[0]
    P2=vertex[1]
    P3=vertex[2]
    P4=vertex[3]
    try:
        diogonal_points_sum=tuple(map(operator.add, P1, P3))
    except:
        diogonal_points_sum=tuple(map(operator.add, P2, P4))

    point=tuple(p/2 for p in diogonal_points_sum)
    
    try:
        triangle1=Plane(Point3D(P1),Point3D(P2),Point3D(P3))
    except:
        triangle1=np.nan
    try:
        triangle2=Plane(Point3D(P1),Point3D(P3),Point3D(P4))
    except:
        triangle2=np.nan
    try:
        triangle3=Plane(Point3D(P1),Point3D(P2),Point3D(P4))
    except:
        triangle3=np.nan
    try:
        triangle4=Plane(Point3D(P2),Point3D(P3),Point3D(P4))
    except:
        triangle4=np.nan
        
    try:
        norm1=triangle1.normal_vector
    except:
        norm1=np.nan
    try:
        norm2=triangle2.normal_vector
    except:
        norm2=np.nan
    try:
        norm3=triangle3.normal_vector
    except:
        norm3=np.nan
    try:
        norm4=triangle4.normal_vector
    except:
        norm4=np.nan
        
    if pd.isnull(norm1)==False:
        if pd.isnull(norm2)==False:
            vector=tuple(map(operator.add, norm1, norm2))
        else:
            vector=norm1
    elif pd.isnull(norm2)==False:
        vector=norm2
    elif pd.isnull(norm3)==False:
        vector=norm3
    else:
        vector=norm4
                
    return point,vector
                
#*****************************************************************************
#*****************************************************************************
# DIS3D Input maker for the source ! 
#*****************************************************************************
#*****************************************************************************
def dis3dpatchMaker(patchL,inc_meter,X_meter,Y_meter,Z_meter,S_meter,const_rake_az,rotationAngle,name):
    time=datetime.datetime.now()
    MinSlipLim=0.0001
    if pd.isnull(rotationAngle):
        outputfilepath=os.path.abspath("outputs/global-mesh")
        dis3dfaultfilename="dis3dsource_%s_ONgrid%ix%ikm_patchL%ikm.txt"%(name,inc_meter/1000,inc_meter/1000,patchL/1000)
    else:
        outputfilepath=os.path.abspath("outputs/rotated-mesh") 
        dis3dfaultfilename="dis3dsource_%s_ONgrid%ix%ikm_patchL%ikm_using%iRotMesh.txt"%(name,inc_meter/1000,inc_meter/1000,patchL/1000,rotationAngle)
    S=S_meter
    H=(patchL/1000)/2 # has to be in km
    xx0=[]
    yy0=[]
    zz0=[]
    nn1=[]
    nn2=[]
    nn3=[]
    rr1=[]
    rr2=[]
    rr3=[]
    xi=X_meter 
    yi=Y_meter 
    Z=Z_meter #negative
    dis3dfaultfile=open(os.path.join(outputfilepath,dis3dfaultfilename),'w')
    
    K=0
    k=0
    for i in range(len(Z)-1):
        for j in range(len(Z[0])-1):
            if not np.isnan(S[i][j]):
                if S[i][j]>MinSlipLim:
                    if not np.isnan(Z[i][j]):P1=(xi[i][j],yi[i][j],Z[i][j])
                    else:P1=np.nan
                    if not np.isnan(Z[i][j+1]):P2=(xi[i][j+1],yi[i][j+1],Z[i][j+1])
                    else:P2=np.nan
                    if not np.isnan(Z[i+1][j+1]):P3=(xi[i+1][j+1],yi[i+1][j+1],Z[i+1][j+1])
                    else:P3=np.nan
                    if not np.isnan(Z[i+1][j]):P4=(xi[i+1][j],yi[i+1][j],Z[i+1][j])
                    else:P4=np.nan
                    vertex=[P1,P2,P3,P4]
                    if sum([pd.isnull(v) for v in vertex])<2:
                        point,vector=plane3D_normal(vertex) 
                        print('X%i Y%i : At least 3 vertex were found'%(j,i))
                        s=np.sqrt(float(np.power(vector[0],2)+np.power(vector[1],2)+np.power(vector[2],2)))
                        norm=list(v/s for v in vector) 
        
                        if norm[2]>=0:
                            print('.......normal to patch is upward!')
                            strikevector=np.cross([0,0,1],norm)
                            dipvector=np.cross(strikevector,norm)
                        else:
                            print('.......STOPED because the normal to patch was downward! it should not!!\n The patch vertex are:!')
                            print(vertex)
                            return None

                            
                        strikevectorL=np.sqrt(float(np.dot(strikevector,strikevector)))
                        strike=list(v/strikevectorL for v in strikevector)
                        dipvectorL=np.sqrt(float(np.dot(dipvector,dipvector))) 
                        dip=round(np.degrees(np.pi/2-math.acos(np.dot(list(v/dipvectorL for v in dipvector),[0,0,-1]))),1)
                        azimuth=round(np.degrees(math.acos(np.dot(strike,[0,1,0]))),1)
                        if strike[0]<0:azimuth=360-azimuth
                        
                        if dip <0 or dip>90:
                            thet=-1
                            print('.......BAD DIP ESTIMATION! CHECK IT! dip is found > 90 or negative! Patch discarded!')
                        elif dip<1.5:
                            thet=1.5
                            print('.......SPECIAL CORRECTION! Dip was too small (< 1.5 degree)! dip is given 1.5')                              
                        else: 
                            thet=dip
                            print('.......the patch is OK!')
                        if not thet<1:    
                            k=k+1                                
                            const_rake_angle=np.radians(-const_rake_az+90)
                            perpendicularTo_slip_vector=(np.cos(const_rake_angle+np.pi/2),np.sin(const_rake_angle+np.pi/2),0)
    
                            plane_slip_vector=np.cross(perpendicularTo_slip_vector,norm)
                            plane_slip_vectorL=np.sqrt(float(np.dot(plane_slip_vector,plane_slip_vector)))    
                            slip_dip_angle=np.degrees(math.acos(np.dot(list(v/dipvectorL for v in dipvector),list(v/plane_slip_vectorL for v in plane_slip_vector))))
                            slip_strike_angle=np.degrees(math.acos(np.dot(list(v/strikevectorL for v in strikevector),list(v/plane_slip_vectorL for v in plane_slip_vector))))
                            
                            if slip_dip_angle>90:
                                rake_angle=slip_strike_angle
                                print('........rake is reverse: %.2f degree'%rake_angle)
                            elif slip_dip_angle==90.00:
                                rake_angle=slip_strike_angle
                                print('........rake is pure strike-slip: %.2f degree'%rake_angle) 
                            else:
                                rake_angle=-slip_strike_angle
                                print('........rake is normal: %.2f degree'%rake_angle)                        

                            phi=azimuth                                  
                            # half of increment along dip in projected plane is H
                            # but in real fault plane is E
                            E=H/np.cos(np.radians(thet)) #is positive and in km                                
                            #each point will be considerd as the center point of each segments
                            DU=-1*(((point[2]/1000)+E*np.sin(np.radians(thet)))/np.sin(np.radians(thet))) #is positive and in km
                            DL=-1*(((point[2]/1000)-E*np.sin(np.radians(thet)))/np.sin(np.radians(thet))) #is positive and in km
                            R=(DU+E)*np.cos(np.radians(thet)) #is positive and in km
                            angleR=180-phi
                            deltaX1=R*np.sin(np.radians(angleR)) # in km
                            deltaX2=R*np.cos(np.radians(angleR)) # in km
                            x1G=point[1]/1000+deltaX1
                            x2G=point[0]/1000+deltaX2                                
                            DS=-1*np.sin(np.radians(rake_angle))*S[i][j] # has to be in meter in dis3d input
                            SS=np.cos(np.radians(rake_angle))*S[i][j] # has to be in meter in dis3d input
                            if abs(DS)>MinSlipLim or abs(SS)>MinSlipLim:
                                K=K+1
                                if pd.isnull(rotationAngle):
                                    dis3dfaultfile.writelines('  %i   %.2f   %.2f   %.2f   %.2f  %.2f   %.2f   %.2f   %.4f   %.4f   0\n'%(K,H,DU,DL,thet,phi,x1G,x2G,SS,DS))
                                    a,b=point[0],point[1]
                                    c,d=norm[0],norm[1]
                                    e,f=plane_slip_vector[0],plane_slip_vector[1]
                                else:                            
                                    phi=azimuth-rotationAngle
                                    x2G_r,x1G_r=rotate((0,0),(x2G,x1G),np.radians(rotationAngle))
                                    dis3dfaultfile.writelines('  %i   %.2f   %.2f   %.2f   %.2f  %.2f   %.2f   %.2f   %.4f   %.4f   0\n'%(K,H,DU,DL,thet,phi,x1G_r,x2G_r,SS,DS)) 
                                    a,b=rotate((0,0),(point[0],point[1]),np.radians(rotationAngle))
                                    c,d=rotate((0,0),(norm[0],norm[1]),np.radians(rotationAngle)) 
                                    e,f=rotate((0,0),(plane_slip_vector[0],plane_slip_vector[1]),np.radians(rotationAngle))  
                                
                                xx0.append(a)
                                yy0.append(b)
                                zz0.append(point[2])
                                nn1.append(c)
                                nn2.append(d)
                                nn3.append(norm[2])
                                rr1.append(e/plane_slip_vectorL)
                                rr2.append(f/plane_slip_vectorL)
                                rr3.append(plane_slip_vector[2]/plane_slip_vectorL)
                            else:
                                print('......... OH! the slip for this patch is too small!')                                                                
                    else:
                        print('X%i Y%i : OPS! Not enough vertex'%(j,i))
                else:
                    print('X%i Y%i : OPS! the slip for this patch is zero!')
            else:
                print('X%i Y%i : OPS! No slip value!'%(j,i))
                            
    dis3dfaultfile.writelines('PRINT')
    dis3dfaultfile.close()
    print('%i OK patches with slip value'%k)
    print('%i OK patches with ss or ds bigger than %.5f meter'%(K,MinSlipLim))
    print('Elapsed time for making source patched : %s'%(datetime.datetime.now()-time))
    allnormV=np.array([xx0,yy0,zz0,nn1,nn2,nn3,rr1,rr2,rr3])
    return allnormV

#*****************************************************************************
#*****************************************************************************
# DIS3D Input reader and mapper for making sure ! 
#*****************************************************************************
#*****************************************************************************
def dis3dpatchMapper(file,param) :
    f=open(file)
    lines=f.readlines()[:-1]
    f.close()
    vertex3D=[]
    vertex2D=[]
    TotalSlip=[]
    ALLSS=[]
    ALLDS=[]
    ALLstrike=[]
    ALLdip=[]
    xx=[]
    yy=[]
    zz=[]
    for line in lines:
        H=float(line.split()[1])#*1000
        DU=float(line.split()[2])#*1000
        DL=float(line.split()[3])#*1000
        dip=float(line.split()[4])
        strike=float(line.split()[5])
        Y=float(line.split()[6])#*1000
        X=float(line.split()[7])#*1000
        SS=float(line.split()[8])
        DS=float(line.split()[9])
        ALLstrike.append(strike) 
        ALLdip.append(dip)
        ALLSS.append(SS)        
        ALLDS.append(DS)
        TotalSlip.append(np.sqrt(DS*DS+SS*SS))
        
        dus=DU*np.cos(np.radians(dip))
        dls=DL*np.cos(np.radians(dip))
        zup=-DU*np.sin(np.radians(dip))
        zz.append(zup)
        zdown=-DL*np.sin(np.radians(dip))
        dipVec_z=zdown-zup
        xup=X+dus*np.cos(np.radians(-1*strike))
        xx.append(xup)
        xdown=X+dls*np.cos(np.radians(-1*strike))
        dipVec_x=xdown-xup
        yup=Y+dus*np.sin(np.radians(-1*strike))
        yy.append(yup)
        ydown=Y+dls*np.sin(np.radians(-1*strike))
        dipVec_y=ydown-yup
        
        stVec_x=X+H*np.cos(np.radians(-1*strike+90))-X
        stVec_y=Y+H*np.sin(np.radians(-1*strike+90))-Y
        
        p1=np.add((xup,yup,zup),(stVec_x,stVec_y,0))
        p2=np.add(p1,(dipVec_x,dipVec_y,dipVec_z))
        p3=np.add(p2,(-2*stVec_x,-2*stVec_y,0))
        p4=np.add(p3,(-1*dipVec_x,-1*dipVec_y,-1*dipVec_z))
        
        x=[p1[0],p2[0],p3[0],p4[0]]
        y=[p1[1],p2[1],p3[1],p4[1]]
        z=[p1[2],p2[2],p3[2],p4[2]]
        vertex3D.append([list(zip(x,y,z))])
        vertex2D.append([list(zip(x,y))])
    limits=[int(min(xx)),int(max(xx)),int(min(yy)),int(max(yy)),int(min(zz)),int(max(zz))]
    
    if param=='strike':
        value=ALLstrike
        valuerange=[0,360]
    elif param=='dip':
        value=ALLdip
        valuerange=[0,90]
    elif param=='total_slip':
        value=TotalSlip
        valuerange=[0,0.25]
    elif param=='dip_slip':
        value=ALLDS
        valuerange=[-0.15,0.15]
    elif param=='strike_slip':
        value=ALLSS
        valuerange=[-0.15,0.15]
    else:
        print('The given parameter is not defined')
        return None

    return vertex3D,vertex2D,value,valuerange,limits
#*****************************************************************************
#*****************************************************************************
# DIS3D Input reader and mapper for making sure ! 
#*****************************************************************************
#*****************************************************************************
def dis3dpatchIndexCorrector(filename,filepath,index,percentage) :
    if index>0:
        newfilename='%s_startIndex%i_%iPerc.txt'%(filename[:-4],index,percentage)
    else:
        newfilename='%s_%iPerc.txt'%(filename[:-4],percentage)
    f=open(os.path.join(filepath,filename),'r')
    new=open(os.path.join(filepath,newfilename),'w')
    lines=f.readlines()[:-1]
    f.close()
    K=index
    for line in lines:
        H=float(line.split()[1])
        DU=float(line.split()[2])
        DL=float(line.split()[3])
        dip=float(line.split()[4])
        strike=float(line.split()[5])
        Y=float(line.split()[6])
        X=float(line.split()[7])
        SS=float(line.split()[8])*(percentage/100)
        DS=float(line.split()[9])*(percentage/100)
        if abs(SS)>0.0001 or abs(DS)>0.0001:
            K=K+1
            new.writelines('  %i   %.2f   %.2f   %.2f   %.2f  %.2f   %.2f   %.2f   %.4f   %.4f   0\n'%(K,H,DU,DL,dip,strike,Y,X,SS,DS))
    new.writelines('PRINT')
    new.close()
    return     
    



















