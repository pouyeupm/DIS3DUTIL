#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 23 21:02:40 2020
by Pouye Yazdi
"""
import os
import pyproj
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

# ===========================================================================
# Make a VERTICAL GRID with Dis3D output format 
# ===========================================================================
def dis3dCalc_ProfileMaker_V(gridsfilepath,profile_azimuth,uppercenter_lat,uppercenter_long,length,dl,width,dw,nickname,strike,dip):
    name='target-verticalProfile-%ix%ikm-%ix%ikm-az%i-%s-for-s%s-d%s.txt'%(dl,dw,2*length,width,profile_azimuth,nickname,strike,dip)
    calc_verticalfile=open(os.path.join(gridsfilepath,name),'w')
    theta=np.radians(profile_azimuth)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c,s), (-s, c)))
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    uppercenter_x,uppercenter_y=p(uppercenter_long,uppercenter_lat)
    k=0
    yi=np.arange(-1*length,length+dl,dl)
    zi=np.arange(0,width+dw,dw)
    xlist=[]
    ylist=[]
    for z in zi:
        for y in yi:
            k=k+1
            xr,yr=np.dot(R,[0,y])
            X1=yr+uppercenter_y/1000
            X2=xr+uppercenter_x/1000
            X3=z
            calc_verticalfile.writelines('CO1\n  %i   %.2f   %.2f   %.2f\nDISP\nPRINT\nSTRESS\nFTRN\n  %.1f   %.2f\nPRINT\n'%(k,X1,X2,X3,strike,dip))
            if z==zi[0]:
                xlist.append(X2)
                ylist.append(X1)
    calc_verticalfile.close()
    plt.scatter(xlist,ylist) 
    plt.scatter(xlist[0],ylist[0]) 
    return [xlist,ylist]

# ===========================================================================
# Make a HORIZONTAL GRID with Dis3D output format
# ===========================================================================
def dis3dCalc_ProfileMaker_H(filepath,minlon,maxlon,minlat,maxlat,incx_km,incy_km,depth_km,nickname,strike,dip):    
    name='target-horizontalProfile-%ix%ikm-%s-for-s%s-d%s.txt'%(incx_km,incy_km,nickname,strike,dip)
    calc_horizontalfile=open(os.path.join(filepath,name),'w')
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    minx,miny=p(minlon,minlat)
    maxx,maxy=p(maxlon,maxlat)
    k=0
    xi=np.arange(minx,maxx,incx_km*1000)
    yi=np.arange(miny,maxy,incy_km*1000)
    for j in yi:
        for i in xi:
            k=k+1
            calc_horizontalfile.writelines('CO1\n  %i   %.2f   %.2f   %.2f\nDISP\nPRINT\nSTRESS\nFTRN\n  %.1f   %.2f\nPRINT\n'%(k,j/1000,i/1000,depth_km,strike,dip))
    calc_horizontalfile.close()

# ===========================================================================
# Make a GRID with Dis3D output format over the subduction
# ===========================================================================
def dis3dCalc_interface(filepath,incl_km,incw_km,strike,dip):    
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    subductionfile=open(os.path.join(filepath,'interface3D.txt'),'r')
    suductiongrid=subductionfile.readlines()
    subductionfile.close()

    zs=[]
    xs_utm=[]
    ys_utm=[]
    for grid in suductiongrid:
        lon_grid=round(float(grid.split()[0]),3)
        lat_grid=round(float(grid.split()[1]),3)
        zs.append(round(float(grid.split()[2]),3))# in km and positive
        xs_utm.append(p(lon_grid,lat_grid)[0])# in metre in Global system
        ys_utm.append(p(lon_grid,lat_grid)[1])# in metre in Global system
    x_range=np.arange(min(xs_utm),max(xs_utm),incl_km*1000)
    y_range=np.arange(min(ys_utm),max(ys_utm),incl_km*1000)
    X,Y=np.meshgrid(x_range,y_range)     
    Z=griddata((xs_utm,ys_utm),zs,(X,Y),method='linear')
    x2=list(np.reshape(X,(1,len(x_range)*len(y_range))))
    x1=list(np.reshape(Y,(1,len(x_range)*len(y_range))))
    x3=list(np.reshape(Z,(1,len(x_range)*len(y_range))))
    
    name='target_subductionInterface-%ix%ikm-for-s%s-d%s.txt'%(incl_km,incw_km,strike,dip)
    calc_interfacefile=open(os.path.join(calcGridsfilepath,name),'w')
    k=0
    for i in range(len(x1[0])):
        if not np.isnan(x3[0][i]):
            k=k+1
            calc_interfacefile.writelines('CO1\n  %i   %.2f   %.2f   %.2f\nDISP\nPRINT\nSTRESS\nFTRN\n  %.1f   %.2f\nPRINT\n'%(k,x1[0][i]/1000,x2[0][i]/1000,x3[0][i],strike,dip))
    calc_interfacefile.close()
    return 