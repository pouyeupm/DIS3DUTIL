#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 09:25:51 2018
by Pouye Yazdi
pouye.yazdi@upm.es
"""

import os
import numpy as np
from math import sin,cos,radians
import pyproj

def stressReader(filename,filepath,mu,rake,linenumber):
    stressfile=open(os.path.join(filepath,filename))
    stresses=stressfile.readlines()[linenumber:]
    stressfile.close()
    # =============================================================================
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    # =============================================================================
    # Read stress results from dis3D outputs
    # =============================================================================
    i=0 
    stress=[]
    print(len(stresses))
    while (i < len(stresses)):
        stress.append(stresses[i])
        i=i+16 
    Zkm=[]
    LON=[]
    LAT=[]
    Xkm=[]
    Ykm=[]
    Coulomb=[]
    for i in range(len(stress)):
        ykm=round(float(stress[i].split()[0]),2)
        xkm=round(float(stress[i].split()[1]),2)
        Xkm.append(xkm)
        Ykm.append(ykm)
        lon,lat=p(xkm*1000,ykm*1000,inverse=True)
        LON.append(lon) # longitude
        LAT.append(lat) # latitude
        zkm=-1*round(float(stress[i].split()[2]),2)
        Zkm.append(zkm) # km and negative
        
#        S11=float(stress[i].split()[3]) #along y (or X1) and perpendicular a xz
        S22=float(stress[i].split()[4]) #along x (or X2) and perpendicular to rupture plane --if is positive it is extentional
#        S33=float(stress[i].split()[5]) #along z (or X3) and perpendicular a xy
        S12=float(stress[i].split()[6]) #along y and perpendicular a yz
#        S13=float(stress[i].split()[7]) #along y and perpendicular a xy
        S23=float(stress[i].split()[8]) #along x and perpendicular a xz
           
        SC=S12*cos(radians(rake))-S23*sin(radians(rake))
        Coulomb.append(SC+mu*S22)
    return Xkm,Ykm,Zkm,LON,LAT,Coulomb

# ===========================================================================
# 
# ===========================================================================
"""
This code use catalog in SEDA format.
      year month day hour minute second lat long depth mag
 For 3d plot of Coco subduction a 3d scattered grid is needed
      long lat depth(positive) 
"""
def eventReaderll(file):
    cat=open(file,'r')
    events=cat.readlines()
    cat.close()
    lat=[]
    long=[]
    mag=[]
    for event in events:
        """
        if round(float(event.split()[9]),1)<6.2:
            mag.append(round(float(event.split()[9]),1))
            depth=-1*round(float(event.split()[8]),1)            
            lat.append(round(float(event.split()[6]),3))
            long.append(round(float(event.split()[7]),3))
        """
        mag.append(round(float(event.split()[9]),1))
        depth=-1*round(float(event.split()[8]),1)            
        lat.append(round(float(event.split()[6]),3))
        long.append(round(float(event.split()[7]),3))
        
    return long,lat,mag
# ===========================================================================
# Read ASCII file related to slip (SSE or interSSE) (format type by M.R)
# ===========================================================================
def slipfileReader(file):
    slipfile=open(file,'r')
    lines=slipfile.readlines()
    slipfile.close()
    
    totalslip=[]
    long=[]
    lat=[]
    depth=[]
    degrake=[]
    for i in range(len(lines)):
        rake=round(float(lines[i].split()[3]),3) #in degree
        degrake.append(rake)
        strikeslip=round(float(lines[i].split()[4]),6) #in meter
        dipslip=round(float(lines[i].split()[5]),6) #in meter
        totalslip.append(np.sqrt((strikeslip*strikeslip)+(dipslip*dipslip))) #in meter
        long.append(round(float(lines[i].split()[0]),3))
        lat.append(round(float(lines[i].split()[1]),3))
        depth.append(round(float(lines[i].split()[2]),3)*-1000)#in meter and negative
    print('There are %i points with slip value'%len(totalslip))
    return long,lat,depth,degrake,totalslip

