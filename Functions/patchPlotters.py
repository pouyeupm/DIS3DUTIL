#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 18:52:15 2020
by Pouye Yazdi
"""

import os
import pyproj
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from basepolygons import basemapPolysxykm
from patchMakers import dis3dpatchMapper
## ==========================================================================
## ==========================================================================   
def patch_plot_color():      
    bounds=np.arange(0,0.25,0.001)
    boundsleg=np.arange(0,0.25,0.02)
    cmap = plt.cm.nipy_spectral_r
    cmaplist = [cmap(i) for i in range(int(218))]+[plt.cm.CMRmap(i) for i in range(15,int(230))]
    #cmaplist = [cmap(i) for i in range(int(220))]
    cmaplist=cmaplist[:int(len(cmaplist))]
    cmap = cmap.from_list('Custom cmap', cmaplist, len(bounds))    
    return cmap,bounds,boundsleg
## ==========================================================================
## ==========================================================================   
def patch_plot_color_0(limits):
    if limits[1]==360:
        bounds=np.arange(0,360,1)
        boundsleg=np.arange(0,360,20)
        cmap = plt.cm.gnuplot_r
        cmaplist = [cmap(i) for i in range(int(250))]
    elif limits[1]==90:
        bounds=np.arange(0,90,1)
        boundsleg=np.arange(0,90,10)
        cmap = plt.cm.brg
        cmaplist = [cmap(i) for i in range(int(250))]
    elif limits[0]<0:
        bounds=np.arange(limits[0],limits[1],0.001)
        boundsleg=np.arange(limits[0],limits[1],0.02)
        cmap = plt.cm.seismic
        cmaplist = [cmap(i) for i in range(int(255))]
    else:
        bounds=np.arange(limits[0],limits[1],0.001)
        boundsleg=np.arange(limits[0],limits[1],0.02)
        cmap = plt.cm.nipy_spectral_r
        cmaplist = [cmap(i) for i in range(int(218))]+[plt.cm.CMRmap(i) for i in range(15,int(230))]
    cmaplist=cmaplist[:int(len(cmaplist))]
    cmap = cmap.from_list('Custom cmap', cmaplist, len(bounds))    
    return cmap,bounds,boundsleg
### =========================================================================
### ========== 3D ============================================================
### =========================================================================
def patch_plot_3Dkm(patches_3D,param,paramlimits,limits,title,plotVariable):
    
    
    cmap,bounds,boundsleg=patch_plot_color_0(paramlimits)
    norm = matplotlib.colors.BoundaryNorm(bounds, len(bounds)) 
    m = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    m.set_array([])
    fcolors_rm_patch = m.to_rgba(param)
    
    fig=plt.figure(figsize=(9,6))
    ax=fig.add_subplot(111,projection='3d',adjustable='box')
    ### ========== The main SSE data:
    #from readingSSE import scatter_x,scatter_y,scatter_z,triang
    #ax.plot_trisurf(triang,scatter_z,alpha=0.2)
    #ax.scatter(scatter_x, scatter_y,scatter_z, marker='.', s=10, c="red", alpha=0.5)
    ### ========== The normal to plane and slip vectors estimated while constructing dis3d patches
    #O1,O2,O3,V1,V2,V3,S1,S2,S3=zip(allnormV)
    #plt.quiver(O1[0],O2[0],O3[0],S1[0],S2[0],S3[0],length=20000,color='r')
    #plt.quiver(O1[0],O2[0],O3[0],V1[0],V2[0],V3[0],length=10000,color='r')
    
    for i in range(len(patches_3D)):
        pc = Poly3DCollection(patches_3D[i], linewidths=2)
        pc.set_alpha(0.8) 
        pc.set_facecolor(fcolors_rm_patch[i])
        pc.set_clim(paramlimits[0],paramlimits[1])
        ax.add_collection3d(pc)
    ax.set_xlim([limits[0],limits[1]])
    ax.set_ylim([limits[2],limits[3]])
    ax.set_zlim([limits[4],limits[5]])
    ax.set_title('Patch file: \n %s'%title)
    ax.set(xlabel='Longitude (km)',ylabel='Latitude (km)',zlabel='depth (km)')
    #ax.axis('equal')
    
    ax.view_init(61,120)
    ax2= fig.add_axes([0.1, 0.2, 0.02, 0.6])
    if plotVariable[-4:]=='slip':plotVariable=plotVariable+' (m)'
    ax2.set_title(label=plotVariable,fontname = "Courier New",fontsize=10)
    matplotlib.colorbar.ColorbarBase(ax2,cmap=cmap,boundaries=bounds,
                            ticks=boundsleg,
                            orientation='vertical')
    for tick in ax2.get_yticklabels():
        tick.set_fontsize(10)
        tick.set_family("Courier New")    
    plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.1)


### =========================================================================
### ========== 2D ===========================================================
### =========================================================================

def patch_plot_2Dkm(patches_2D,param,paramlimits,limits,title,plotVariable):
    land,poly=basemapPolysxykm(-102,-99,16,18.5) 
    
    cmap,bounds,boundsleg=patch_plot_color_0(paramlimits)
#    cmap,bounds,boundsleg=patch_plot_color()
    
    fig=plt.figure(figsize=(9,6))
    ax=fig.add_subplot(111)
    patches=[]
    for i in range(len(patches_2D)):
        polygon=Polygon([list(patches_2D[i][0][j]) for j in range(4)])
        patches.append(polygon)
    p = PatchCollection(patches,cmap=cmap,alpha=0.9)
    p.set_array(np.array(param))
    p.set_clim(paramlimits[0],paramlimits[1])
    ax.add_collection(p)
    ax.set(xlabel='Longitude (km)',ylabel='Latitude (km)')
    proj=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    
    
##### ---- For Covering all data --------------------
#    ax.set_xlim([limits[0],limits[1]])
#    ax.set_ylim([limits[2],limits[3]])
##### ------------------------------------------------
    
    
##### ----Example coordinates for the presebtation----
#    limits[0],limits[2]=proj(-99,15)
#    limits[1],limits[3]=proj(-95,18)
#    ax.set_xlim([limits[0]/1000,limits[1]/1000])
#    ax.set_ylim([limits[2]/1000,limits[3]/1000])
##### ------------------------------------------------
    
    
##### ---- Coordinates for Coyuca --------------------
    limits[0],limits[2]=proj(-102,16)
    limits[1],limits[3]=proj(-99,18.5)
    ax.set_xlim([limits[0]/1000,limits[1]/1000])
    ax.set_ylim([limits[2]/1000,limits[3]/1000])
##### ------------------------------------------------
    
    
    ax.set_title('Patch file: \n %s'%title)
    ax.plot([cor[0] for cor in land[:-3]],[cor[1] for cor in land[:-3]],'k-')
    ax2= fig.add_axes([0.16, 0.2, 0.02, 0.6])
    if plotVariable[-4:]=='slip':plotVariable=plotVariable+' (m)'
    ax2.set_title(label=plotVariable,fontname = "Courier New",fontsize=12)
    matplotlib.colorbar.ColorbarBase(ax2,cmap=cmap,boundaries=bounds,
                            ticks=boundsleg,
                            orientation='vertical')
    for tick in ax2.get_yticklabels():
        tick.set_fontsize(12)
        tick.set_family("Courier New")    
    plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.1)
    
### =========================================================================
### ========== PLOT 2D and 3D ===============================================
### =========================================================================  
    
def plotPaches(filename,outfilepath,parameter):

    header=filename[12:-4]  
    if 'RotMesh' in filename:
        filepath=os.path.join(outfilepath,'rotated-mesh')   
    else:
        filepath=os.path.join(outfilepath,'global-mesh')   
    file=os.path.join(filepath,filename) 
    patches3D,patches2D,plottingParam,ParamLimits,lims=dis3dpatchMapper(file,parameter)    
    patch_plot_3Dkm(patches3D,plottingParam,ParamLimits,lims,header,parameter)
    patch_plot_2Dkm(patches2D,plottingParam,ParamLimits,lims,header,parameter)