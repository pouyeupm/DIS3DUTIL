#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 09:47:01 2018
by Pouye Yazdi
pouye.yazdi@upm.es
"""

from mpl_toolkits.basemap import Basemap
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import pyproj
import numpy as np



def basemapPolysll(minlon,maxlon,minlat,maxlat):

    map = Basemap(projection='merc',ellps = 'WGS84',
                      llcrnrlat=minlat,
                      urcrnrlat=maxlat,
                      llcrnrlon=minlon,
                      urcrnrlon=maxlon,
                      resolution='i',
                      epsg='4326')        
    landpoly=map.coastpolygons[0]
    pointlandpoly=[]
    for i in range(len(landpoly[0])):
        pointlandpoly.append([landpoly[0][i],landpoly[1][i]])
    try:
        lakepoly=map.coastpolygons[1]
        pointlakepoly=[]
        for i in range(len(lakepoly[0])):
            pointlakepoly.append([lakepoly[0][i],lakepoly[1][i]])    
    except:
        pointlakepoly=[] 
        
    return pointlandpoly,pointlakepoly

def basemapPolysxym(minlon,maxlon,minlat,maxlat):

    map = Basemap(projection='merc',ellps = 'WGS84',
                      llcrnrlat=minlat,
                      urcrnrlat=maxlat,
                      llcrnrlon=minlon,
                      urcrnrlon=maxlon,
                      resolution='i',
                      epsg='4326')


    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')    
    landpoly=map.coastpolygons[0]
    pointlandpoly=[]
    for i in range(len(landpoly[0])):
        pointlandpoly.append(p(landpoly[0][i],landpoly[1][i]))
    try:
        lakepoly=map.coastpolygons[1]
        pointlakepoly=[]
        for i in range(len(lakepoly[0])):
            pointlakepoly.append(p(lakepoly[0][i],lakepoly[1][i]))
    except:
        pointlakepoly=[]
   
    return pointlandpoly,pointlakepoly


def basemapPolysxykm(minlon,maxlon,minlat,maxlat):

    map = Basemap(projection='merc',ellps = 'WGS84',
                      llcrnrlat=minlat,
                      urcrnrlat=maxlat,
                      llcrnrlon=minlon,
                      urcrnrlon=maxlon,
                      resolution='i',
                      epsg='4326')


    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')    
    landpoly=map.coastpolygons[0]
    pointlandpoly=[]
    for i in range(len(landpoly[0])):
        pointlandpoly.append([p(landpoly[0][i],landpoly[1][i])[0]/1000,p(landpoly[0][i],landpoly[1][i])[1]/1000])
    try:
        lakepoly=map.coastpolygons[1]
        pointlakepoly=[]
        for i in range(len(lakepoly[0])):
            pointlakepoly.append([p(lakepoly[0][i],lakepoly[1][i])[0]/1000,p(lakepoly[0][i],lakepoly[1][i])[1]/1000])
    except:
        pointlakepoly=[]
        
    return pointlandpoly,pointlakepoly


def basemapPolysll3d(minlon,maxlon,minlat,maxlat,depth):

    map = Basemap(projection='merc',ellps = 'WGS84',
                      llcrnrlat=minlat,
                      urcrnrlat=maxlat,
                      llcrnrlon=minlon,
                      urcrnrlon=maxlon,
                      resolution='i',
                      epsg='4326')
  
    landpoly=map.coastpolygons[0]
    pointlandpoly=[list(zip(landpoly[0],landpoly[1],np.arange(len(landpoly[0]))*0+depth))]
    try:
        lakepoly=map.coastpolygons[1]   
        pointlakepoly=[list(zip(lakepoly[0],lakepoly[1],np.arange(len(lakepoly[0]))*0+depth))]
    except:
        pointlakepoly=[]

    return pointlandpoly,pointlakepoly

def basemapPolysxykm3d(minlon,maxlon,minlat,maxlat,depth):
    map = Basemap(projection='merc',ellps = 'WGS84',
                      llcrnrlat=minlat,
                      urcrnrlat=maxlat,
                      llcrnrlon=minlon,
                      urcrnrlon=maxlon,
                      resolution='i',
                      epsg='4326')
    
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')     
    landpoly=map.coastpolygons[0]
    
    landpolyx=[]
    landpolyy=[]
    for i in range(len(landpoly[0])):
        xlandpoly,ylandpoly=p(landpoly[0][i],landpoly[1][i])
        landpolyx.append(xlandpoly/1000)
        landpolyy.append(ylandpoly/1000)
    pointlandpoly=[list(zip(landpolyx,landpolyy,np.arange(len(landpoly[0]))*0+depth))]
    try:
        lakepoly=map.coastpolygons[1]   
        lakepolyx=[]
        lakepolyy=[]
        for i in range(len(lakepoly[0])):
            xlakepoly,ylakepoly=p(lakepoly[0][i],lakepoly[1][i])
            lakepolyx.append(xlakepoly/1000)
            lakepolyy.append(ylakepoly/1000)
        pointlakepoly=[list(zip(lakepolyx,lakepolyy,np.arange(len(lakepoly[0]))*0+depth))]
    except:
        pointlakepoly=[]

    return pointlandpoly,pointlakepoly