#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 00:13:04 2020
by Pouye Yazdi
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import griddata
from basepolygons import basemapPolysll
from patchPlotters import patch_plot_color
from mpl_toolkits.mplot3d import Axes3D
from reader import slipfileReader

slip_plot_color=patch_plot_color

### =========================================================================
### ========== 2D ===========================================================
### =========================================================================

def slip_plot_2Dll(longlist,latlist,sliplist,name,percentage):

    longrange=np.arange(min(longlist),max(longlist),0.2)
    latrange=np.arange(min(latlist),max(latlist),0.2)
#    longrange=np.arange(-99,-95,0.2)
#    latrange=np.arange(15,18,0.2)
    LONG,LAT=np.meshgrid(longrange,latrange)
    SSE=griddata((longlist,latlist),np.dot(sliplist,(percentage/100)),(LONG,LAT),method='linear')
    
    cmap,bounds,boundsleg=slip_plot_color()
    norm = matplotlib.colors.BoundaryNorm(bounds, len(bounds)) 
    
    landpoly,lakepoly=basemapPolysll(min(longlist),max(longlist),min(latlist),max(latlist))
    
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    ax.set(xlabel='Longitude', ylabel='Latitude' ,title='%i%s of %s projection on surface'%(percentage,'$\%$',name))    
    ax.contourf(LONG,LAT,SSE,bounds,cmap=cmap,norm=norm)
    ax.plot([cor[0] for cor in landpoly[:-2]],[cor[1] for cor in landpoly[:-2]],'-k')
    
    ax1= fig.add_axes([0.05, 0.2, 0.02, 0.6])
    ax1.set_title(label='Total Slip\n(m)',fontname = "Courier New",fontsize=12)
    matplotlib.colorbar.ColorbarBase(ax1,cmap=cmap,boundaries=bounds,norm=norm,
                                     ticks=boundsleg,
                                     orientation='vertical') 
    for tick in ax1.get_yticklabels():
        tick.set_fontsize(10)
        tick.set_family("Courier New")     
    plt.subplots_adjust(left=0.2, right=0.9, top=0.9, bottom=0.1)
    ax.grid()
    plt.savefig('%s.png'%name, dpi=300, facecolor='w', edgecolor='w',
                orientation='portrait',format='png')
    
### =========================================================================
### ========== 3D ===========================================================
### =========================================================================
    
def slip_plot_3Dll(longlist,latlist,depthlist,sliplist,name,percentage):

    longrange=np.arange(min(longlist),max(longlist),0.1)
    latrange=np.arange(min(latlist),max(latlist),0.1)
    LONG,LAT=np.meshgrid(longrange,latrange)
    DEPTH=griddata((longlist,latlist),depthlist,(LONG,LAT),method='linear')
    SLIP=griddata((longlist,latlist),np.dot(sliplist,(percentage/100)),(LONG,LAT),method='linear')

    cmap,bounds,boundsleg=slip_plot_color()
    norm = matplotlib.colors.Normalize(0, 0.25)
    m = plt.cm.ScalarMappable(norm=norm, cmap = cmap)
    m.set_array([])
    fcolors = m.to_rgba(SLIP)
   
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111,projection='3d')
    ax.set(xlabel='Longitude', ylabel='Latitude' ,title='%i%s of %s projection on surface'%(percentage,'$\%$',name))    
    ax.plot_surface(LONG,LAT,DEPTH,facecolors=fcolors)

    ax1= fig.add_axes([0.05, 0.2, 0.02, 0.6])
    ax1.set_title(label='Total Slip\n(m)',fontname = "Courier New",fontsize=12)
    matplotlib.colorbar.ColorbarBase(ax1,cmap=cmap,boundaries=bounds,norm=norm,
                                     ticks=boundsleg,
                                     orientation='vertical') 
    for tick in ax1.get_yticklabels():
        tick.set_fontsize(10)
        tick.set_family("Courier New")     
    plt.subplots_adjust(left=0.2, right=0.9, top=0.9, bottom=0.1)
    ax.grid()
    plt.savefig('%s.png'%name, dpi=300, facecolor='w', edgecolor='w',
                orientation='portrait',format='png')

### =========================================================================
### ========== PLOT 2D and 3D ===============================================
### ========================================================================= 

def plotSlip(file,name,percentage):
    out=slipfileReader(file)
    slip_plot_2Dll(out[0],out[1],out[4],name,percentage)
    slip_plot_3Dll(out[0],out[1],out[2],out[4],name,percentage)