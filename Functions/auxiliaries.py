#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  8 21:53:48 2019
by Pouye Yazdi
"""

import numpy as np
import geopy
from geopy.distance import VincentyDistance
from patchMakers import rotate
import matplotlib.tri as mtri
from scipy.interpolate import griddata
import pyproj
from reader import slipfileReader


# ===========================================================================
# Finding the cross section of subduction interface with a vertical profile
# ===========================================================================
def interface3D_crossSection(interface3Dfile,profile_azimuth,uppercenter_lat,uppercenter_long,length,dl):
    theta=np.radians(profile_azimuth)
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c,s), (-s, c)))
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    uppercenter_x,uppercenter_y=p(uppercenter_long,uppercenter_lat)
    k=0
    yi=np.arange(-1*length,length+dl,dl)
    xlist=[]
    ylist=[]
    for y in yi:
        k=k+1
        xr,yr=np.dot(R,[0,y])
        X1=yr+uppercenter_y/1000
        X2=xr+uppercenter_x/1000
        xlist.append(X2)
        ylist.append(X1)
    ##*****************************************************************************    
    subductionfile=open(interface3Dfile,'r')
    suductiongrid=subductionfile.readlines()
    subductionfile.close()
    zs=[]
    xs_utm=[]
    ys_utm=[]
    for grid in suductiongrid:
        lon_grid=round(float(grid.split()[0]),2)
        lat_grid=round(float(grid.split()[1]),2)
        strz=grid.split()[2]
        if not strz=='NaN':
            zs.append(round(float(grid.split()[2]),5)*(-1000))# in metre and negative
            xs_utm.append(p(lon_grid,lat_grid)[0])# in metre in Global system
            ys_utm.append(p(lon_grid,lat_grid)[1])# in metre in Global system
    ##*****************************************************************************  
    xlist1=[l*1000 for l in xlist]
    ylist1=[l*1000 for l in ylist]
    X,Y=np.meshgrid(xlist1,ylist1)  
    Z=griddata((xs_utm,ys_utm),zs,(X,Y),method='linear')
    L=Z.diagonal()
    return L

# ===========================================================================
# Move a (long,lat) toward direction:bearingAngle and with amount:distance
# ===========================================================================
def new_upperCenter_coords(old_long,old_lat,distance,bearingAngle):
    origin = geopy.Point(old_lat,old_long)
    destination = VincentyDistance(kilometers=distance).destination(origin, bearingAngle)
    new_lon,new_lat=destination[1],destination[0]
    return new_lon,new_lat


# =============================================================================
# Finding the cross-section of a fault plane with a vertical plane of a given azimuth
# =============================================================================
def fault_crossSection(l,w,Strike,Dip,profile_azimuth):
    strike=np.radians(90-Strike)
    dip=np.radians(Dip)
    dipV=[np.cos(dip)*np.cos(strike-(np.pi/2)),
          np.cos(dip)*np.sin(strike-(np.pi/2)),
          -1*np.sin(dip)]
    strikeV=[np.cos(strike),np.sin(strike),0]
    az=np.radians(90-profile_azimuth)
    planeV=[np.cos(az-(np.pi/2)),np.sin(az-(np.pi/2)),0]
    
    faultProj=np.cross(np.cross(dipV,strikeV),planeV)
    fl=np.sqrt(faultProj[0]*faultProj[0]+faultProj[1]*faultProj[1])
    fz=faultProj[2]
        
    delta=np.radians(profile_azimuth-(Strike+90))
    a=(np.cos(dip)*w)/(np.abs(np.cos(delta)))
    if faultProj[0]<0: # cross section along given azimuth in km 
        fl=-a
    else:
        fl=a
    fz=np.sin(dip)*w # cross section along Z in km (poistive value)
    fsize=np.sqrt(fl*fl+fz*fz) # size of the cross section in km 
    fault=[fl,fz,fsize]
    return fault

## =============================================================================
## Make a 3D network out of slip files
## =============================================================================
# If points in space are rotated +Alpha (Counterclockwise) It means that the Coordinate system is rotated -Alpha (Clockwise)
# I want the mesh (new coordiante system) rotates -22 degree (-22 degree counterclockwise) --> points will be rotated 22 degrees (22 degree counterclockwise)
# Thus, the rotation angle using function "rotate" has to be 22 for finding the coordinates in rotated mesh (as in lines 49,50)
# And to bring back the coordinates from rotated mech to global mesh, the points has to be rotated again but with angle -22 (as in lines 77,78).

def slipData(file,increment,meshType,roratedmeshAngle):
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    longlist,latlist,depthlist,rakelist,ts=slipfileReader(file)    
    xutm=[]
    yutm=[]
    xutm_r=[]
    yutm_r=[]
    for i in range(len(longlist)):
        xutm_r.append(rotate((0,0),p(longlist[i],latlist[i]),np.radians(-1*roratedmeshAngle))[0]) #in meter
        yutm_r.append(rotate((0,0),p(longlist[i],latlist[i]),np.radians(-1*roratedmeshAngle))[1] )#in meter
        xutm.append(p(longlist[i],latlist[i])[0])#in meter
        yutm.append(p(longlist[i],latlist[i])[1])#in meter
    
    if meshType=='rotated':
        xmeter=xutm_r
        ymeter=yutm_r    
    elif meshType=='global':
        xmeter=xutm
        ymeter=yutm
    print(len(xmeter))
    print(len(ymeter))
    print(len(ts))
    x_range=np.arange(min(xmeter)+increment/2,max(xmeter)+increment/2,increment)
    y_range=np.arange(min(ymeter)+increment/2,max(ymeter)+increment/2,increment)
    X,Y=np.meshgrid(x_range,y_range)     
    TS=griddata((xmeter,ymeter),ts,(X,Y),method='linear')
    Z=griddata((xmeter,ymeter),depthlist,(X,Y),method='linear')  
     
#    scatter_x=[]
#    scatter_y=[]
#    scatter_z=[]
#    scatter_s=[]
#    for i in range(len(y_range)):
#        for j in range(len(x_range)):
#            scatter_x.append(rotate((0,0),(X[i][j],Y[i][j]),np.radians(roratedmeshAngle))[0])
#            scatter_y.append(rotate((0,0),(X[i][j],Y[i][j]),np.radians(roratedmeshAngle))[1])
#            scatter_z.append(Z[i][j])
#            scatter_s.append(TS[i][j])        
#    triang = mtri.Triangulation(scatter_x, scatter_y)
    return X,Y,Z,TS