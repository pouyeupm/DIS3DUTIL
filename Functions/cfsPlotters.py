#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 22:30:08 2020
by Pouye Yazdi
"""
import numpy as np
import pandas as pd
import matplotlib
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from reader import stressReader
from auxiliaries import new_upperCenter_coords
from auxiliaries import fault_crossSection
from auxiliaries import interface3D_crossSection
import pyproj
from reader import eventReaderll
from basepolygons import basemapPolysll,basemapPolysxykm

def cfs_plot_color(maxStress):
    bounds=np.dot(maxStress,[-1.5,-1,-0.5,-0.1,-0.05,-0.01,-0.005,0,0.005,0.01,0.05,0.1,0.5,1,1.5])
    CFSmap = plt.cm.seismic
    cmaplist = [CFSmap(i) for i in range(CFSmap.N)]
    CFSmap = CFSmap.from_list('Custom cmap', cmaplist, CFSmap.N)
    return CFSmap,bounds


def cfs_plot_2Dkm_H(stressfile,stresspath,mu,rake,line,maxStress,earthquakefile):    
    if not pd.isnull(earthquakefile):
        long,lat,mag=eventReaderll(earthquakefile) 
        
    x,y,z,lon,lat,c=stressReader(stressfile,stresspath,mu,rake,line)
    lonrange=np.arange(min(lon),max(lon),0.05)
    latrange=np.arange(min(lat),max(lat),0.05)
    landpoly,lakepoly=basemapPolysll(min(lon),max(lon),min(lat),max(lat))
    CFSmap,bounds=cfs_plot_color(maxStress)
    norm = matplotlib.colors.BoundaryNorm(bounds, CFSmap.N) 
    
    LON,LAT=np.meshgrid(lonrange,latrange)
    C=griddata((lon,lat),c,(LON,LAT),method='linear')
    
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)
    ax.set(xlabel='Longitude', ylabel='Latitude' ,title='$\Delta$ CFS')    
    ax.contourf(LON,LAT,C,bounds,cmap=CFSmap, extend='both',norm=norm)
    ax.plot([cor[0] for cor in landpoly[:-2]],[cor[1] for cor in landpoly[:-2]],'-k')
    ax1= fig.add_axes([0.16, 0.2, 0.7, 0.02])
    matplotlib.colorbar.ColorbarBase(ax1,cmap=CFSmap,boundaries=bounds,extend='both',norm=norm,
                                ticks=np.dot(1,bounds),
                                orientation='horizontal')     
#    ax.scatter([0],[0],s=30,marker='v',facecolor='white',edgecolor='black')   
#    ax.scatter(long,lat,s=17,marker='s',facecolor='yellow',edgecolor='black')
#    ax.grid()
    
def cfs_anim_2Dkm_H(i,args):
    print(i)
    mu,rake,line,ax,CFSfilepath,CFSname,CFSmap,bounds=args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]
    CFSfilename='%s-HorZ%ikm.txt'%(CFSname,i+5)
    x,y,z,lon,lat,c=stressReader(CFSfilename,CFSfilepath,mu,rake,line)
    lonrange=np.arange(min(lon),max(lon),0.05)
    latrange=np.arange(min(lat),max(lat),0.05)
    LONG,LAT=np.meshgrid(lonrange,latrange)
    C=griddata((lon,lat),c,(LONG,LAT),method='linear')
    norm = matplotlib.colors.BoundaryNorm(bounds, CFSmap.N)
    cont = ax.contourf(LONG,LAT,C,bounds,cmap=CFSmap,extend='both',norm=norm)
    ax.set_title('%i: $\Delta$ CFS for depth=%i km'%(i,i+5)) 
    return cont

def cfs_plot_2Dkm_V(stressfile,stresspath,mu,rake,line,maxStress,earthquakefile,epi,faultPlane,subductionLine):
    if not pd.isnull(earthquakefile):
        long,lat,mag=eventReaderll(earthquakefile) 
        
    p=pyproj.Proj(proj='utm',zone=14, ellps='WGS84')
    epix,epiy=p(epi[0],epi[1])
#    EQ=[(x-epix/1000)/(np.sin(np.radians(epi[3]))) for x in xkm]
#    EQ=[(y-epiy/1000)/(np.cos(np.radians(epi[3]))) for y in ykm]
    x,y,z,lon,lat,c=stressReader(stressfile,stresspath,mu,rake,line)
    ## for profiles of size 100x50
    C=np.reshape(c,(16,51))        

    CFSmap,bounds=cfs_plot_color(maxStress)
    norm = matplotlib.colors.BoundaryNorm(bounds, CFSmap.N) 
    
    depths=np.linspace(0,-30,C.shape[0])
    distances=np.linspace(-50,50,C.shape[1])
    Dist,Depth=np.meshgrid(distances,depths)
    
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    ax.set_xlabel('Distance (km)',fontname = "Courier New",fontsize=11)
    ax.set_ylabel('Depth (km)',fontname = "Courier New",fontsize=11)
    ax.set_title('$\Delta$ CFS')
#    plt.axis('equal')
    ax.contourf(Dist,Depth,C,bounds,cmap=CFSmap, extend='both',norm=norm)
    ax.contour(Dist,Depth,C,levels=[0],colors='k',linestyles=':',zorder=8)
    ax1= fig.add_axes([0.1, 0.1, 0.8, 0.02])
    matplotlib.colorbar.ColorbarBase(ax1,cmap=CFSmap,boundaries=bounds,extend='both',norm=norm,
                                ticks=np.dot(1,bounds),
                                orientation='horizontal') 
    ax1.set_title('bar')
    ax.plot(distances,subductionLine,'yellow')
    ax.text(30,1.3,'azimuth=%.0f$^{\circ}$'%epi[3],family="Courier New",fontsize=14,color='b')
    ax.arrow(-50, 0, 97,0, head_width=2, head_length=3, fc='b', ec='b',linewidth=1.5,linestyle='--',zorder=5)     
    ax.scatter(0,0,s=40,marker='o',facecolor='lightblue',edgecolor='black',zorder=7) 
#    
#    ax.scatter(EQ,depth,s=40,marker='o',facecolor='yellow',edgecolor='darkorange',alpha=1,zorder=7) 
    ax.text(0.38, 0.94,'(%.3f , %.3f)'%(epi[0],epi[1]),color='k',family="Courier New",fontsize=12,transform=ax.transAxes)
#    
    ax.set_xlim([-50,50])
    ax.set_ylim([-30,7])
    plt.subplots_adjust(left=0.12, right=0.9, top=0.85, bottom=0.25)
    ax.arrow(0-faultPlane[0]/2, epi[2]-faultPlane[1]/2, faultPlane[0],faultPlane[1], head_width=0, head_length=0, fc='lightblue', ec='k',linewidth=1.5,linestyle='-',zorder=9)
    
def cfs_anim_2Dkm_V(i,args):
    upC_lon,upC_lat=-100.05,17.05
    new_lon,new_lat=new_upperCenter_coords(upC_lon,upC_lat,i-5,22+90)
    subductionLine=interface3D_crossSection(args[10],22,new_lat,new_lon,50,2)
    subductionLine=[s/1000 for s in subductionLine]  
    print(i)
    mu,rake,line,ax,CFSfilepath,CFSname,CFSmap,bounds,depths,distances=args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9]  
    CFSfilename='%s-Ver%ikm.txt'%(CFSname,i-5)
    faultPlane=fault_crossSection(12,16,int(CFSfilename.split('-')[1]),int(CFSfilename.split('-')[2]),22)
    x,y,z,lon,lat,c=stressReader(CFSfilename,CFSfilepath,mu,rake,line)
    C=np.reshape(c,(len(depths),len(distances)))
    norm = matplotlib.colors.BoundaryNorm(bounds, CFSmap.N)
    Dist,Depth=np.meshgrid(distances,depths)
    for line2d in ax.get_lines():
        line2d.remove()
    cont=ax.contourf(Dist,Depth,C,bounds,cmap=CFSmap, extend='both',norm=norm)
    ax.plot(distances,subductionLine,'yellow')
    try:
        ax.findobj(match=type(plt.Text()))[0].remove()
    except:
        s=0        
    ax.text(0.38, 0.94,'(%.3f , %.3f)'%(new_lon,new_lat),color='k',family="Courier New",fontsize=8,transform=ax.transAxes)
    for obj in ax.findobj(lambda x: type(x)==matplotlib.patches.FancyArrow):
        obj.remove()
    deltal=np.tan(np.radians(11))*(i-5)
    ax.arrow(deltal-faultPlane[0]/2, -8-faultPlane[1]/2, faultPlane[0],faultPlane[1], head_width=0, head_length=0, fc='lightblue', ec='k',linewidth=1.5,linestyle='-',zorder=9)
    ax.set_title('%i: $\Delta$CFS for vertical cross-section (%ikm toward ESE)'%(i,i-5))
    return cont
 