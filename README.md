# DIS3DUTIL #

#### This package provides some python modules for making DIS3D more fun! 
The software DIS3D was introduced by Erickson in 1987. However in the aknowlegments he says:
>"Scott Dunbar created the original core program for DlS3D. His code was well organized and logically divided into subroutines, making the program straightforward to adapt and enhance. Scott’s attention to the organization of the original program allowed me to more easily implementm y changes and additionsto the code".

#### About DIS3D program:
This program alculates the displacements, tilts, strains, and stresses due to strike-slip and/or dip-slip motion on any number of __rectangular__ dislocation planes located in a linear elastic half-space. __With the *restriction* that the upper and lower dislocation plane edges are parallel to the surface of the half-space__, the dislocation planes can model faults in the earth with any specified locations, orientations, and dimensions. The program’s input routines allow the user to easily specify individual 

* observation points, 
* horizontal lines of observation points, 
* or grids of observation points 

in any plane of the half-space. 

#### About elastic field calculation:
The calculation of the elastic field around rupture plane located in a half-space (along the earth’s surface and below it) are described by a vector field U and its partial derivatives. The earthquake displacement field U, can be modelled only if we have enough information about the geometry and location of the seismic source, namely the amount of slip along the rupture plane and the elastic properties of the material where the rupture takes place. Converse (1973) derived U(x, y, z) and its partial derivatives for any point (x, y, z) in the elastic half-space for any given elastic moduli. Yang & Davis (1986) formulated U considering opening motion across the rupturing surface too (Erickson, 1987). Okada (1992) also developed the dislocation formula to calculate U(x, y, z) in an elastic half-space with uniform, isotropic elastic properties. All these approaches show that a comprehensive description of the earthquake location, source dimensions and geometry plus its slip model are crucial.

* _Erickson, L. (1987). User’s manual for DIS3D: A three-dimensional dislocation program with applications to faulting in the Earth, Geomechanics. Applied Earth Science Department, Stanford University, Stanford, California._
* _Converse, G. (1973). Equations for the displacements and displacement derivatives due to a rectangular dislocation in a three-dimensional elastic half-space. User’s Manual For DIS3D,U, 119–148._
* _Okada, Y. (1992). Internal deformation due to shear and tensile faults in a half-space. Bull. Seismol. Soc. Am., 82(2), 1018–1040._

#### What is DIS3DUTIL?
If you use DIS3D program, the __DIS3DUTIL__ package might help you making a grid of observation points for any 3D source geometry in the half-space. 

* For example (and as my first case when I developed the DIS3DUTIL modules) if your dislocation plane is a subduction interface which is large in dimention and cannot be presented neither by a single slip value, nor by a single 2D geometry, you will need to make smaller patches out of it. This package lets you do that and construct a DIS3D input from a grid of dislocation points out of your source.

<img src="GuideFigs/1.png" width="700" height="400" />
<img src="GuideFigs/2.png" width="700" height="400" />
<img src="GuideFigs/3.png" width="700" height="400" />
<img src="GuideFigs/4.png" width="700" height="400" />
<img src="GuideFigs/5.png" width="700" height="400" />
<img src="GuideFigs/6.png" width="700" height="400" />
<img src="GuideFigs/7.png" width="700" height="400" />
<img src="GuideFigs/8.png" width="700" height="400" />
<img src="GuideFigs/9.png" width="700" height="400" />
<img src="GuideFigs/10.png" width="700" height="400" />
<img src="GuideFigs/11.png" width="700" height="400" />
<img src="GuideFigs/12.png" width="700" height="400" />
<img src="GuideFigs/13.png" width="700" height="400" />
<img src="GuideFigs/14.png" width="700" height="400" />
