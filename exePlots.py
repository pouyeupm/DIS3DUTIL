#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 20:32:22 2020
by Pouye Yazdi
"""
import os
import sys
sys.path.append('Functions')
import numpy as np

# ===========================================================================
# ...... For 2D and 3D plot of patches (SSE or INTERsse)
# ===========================================================================
filename='dis3dsource_AnnualINTERsse_smoothm0p8_ONgrid15x15km_patchL15km_using-22RotMesh-linear_300Perc.txt' 
parameter='total_slip'
from patchPlotters import plotPaches
from variables import Outputsfilepath
plotPaches(filename,Outputsfilepath,parameter)

# ===========================================================================
# ...... For 2D and 3D plot of slip distribution (SSE or INTERsse)
# ===========================================================================
from variables import SSEfilename,SSEfilepath,SSEname
from variables import INTERssefilename,INTERssefilepath,INTERssename
from slipPlotters import plotSlip
#name=SSEname
#file=os.path.join(SSEfilepath,SSEfilename)
name=INTERssename
file=os.path.join(INTERssefilepath,INTERssefilename)
plotSlip(file,name,100)