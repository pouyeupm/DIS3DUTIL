#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 20:26:15 2020
by Pouye Yazdi
"""
import os
import sys
sys.path.append('Functions')
import numpy as np

# ===========================================================================
# ...... For animating CFS result on vertical cross sections
# ===========================================================================
from variables import CFSfilepath,Inputsfilepath
from cfsPlotters import cfs_anim_2Dkm_V,cfs_plot_color
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib

#CFSname='out-102-36-0.1YearSSE-smoothm0-5x5km'
CFSname='out-89-35-3.0YearINTERSSE-smoothm0p4-5x5km'
#CFSname='out-89-35-0.1and3.0YearSSEandINTERSSE-smoothm0andsmoothm0p4-5x5km'
maximumStress=1
mu=0.4
rake=-90
#line=15109+29 # For SSE smoothm0
#line=13124+29 # For SSE smoothm1
#line=11586+29 # For SSE smoothm2
line=17264+29 # For INTERSSE smoothm0p4 over 5x5km patches
#line=17261+29 # For INTERSSE smoothm0p8 over 5x5km patches
#line=17263+29 # For INTERSSE smoothm0p8-div2 over 5x5km patches
#line=17260+29 # For INTERSSE smoothm1 over 5x5km patches
#line=16999+29 # For INTERSSE smoothm1p4 over 5x5km patches
#line=32373+29 # For 0.1SSEsmoothm0+3INTERSSEsmoothm0p4 over 5x5km patches

subductionfile=os.path.join(Inputsfilepath,'interface3D.txt')
CFSfilespath=os.path.join(CFSfilepath,'89-35--81/out/Ver')
depths=np.linspace(0,-30,16)
distances=np.linspace(-50,50,51)
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.3) 
Cmap,Bounds=cfs_plot_color(maximumStress)
arguments=[mu,rake,line,ax,CFSfilespath,CFSname,Cmap,Bounds,depths,distances,subductionfile]
norm = matplotlib.colors.BoundaryNorm(Bounds, Cmap.N)
ax.set(xlabel='Distance (km)', ylabel='Depth (km)')
ax.grid()
ax.set_xlim([-50,50])
ax.set_ylim([-30,5])
ax.set_aspect('equal', 'box')
ax.text(30,1.3,'azimuth=22$^{\circ}$',family="Courier New",fontsize=8,color='b')
ax.arrow(-50, 0, 97,0, head_width=2, head_length=3, fc='b', ec='b',linewidth=1.5,linestyle='--',zorder=20)
ax.scatter(0,0,s=40,marker='o',facecolor='lightblue',edgecolor='black',zorder=7)      
ax1= fig.add_axes([0.1, 0.12, 0.8, 0.02])
matplotlib.colorbar.ColorbarBase(ax1,cmap=Cmap,boundaries=Bounds,extend='both',norm=norm,
                                ticks=np.dot(1,Bounds),
                                orientation='horizontal') 
for tick in ax1.get_xticklabels():
        tick.set_fontsize(8)
        tick.set_family("Courier New")
ax1.set_title('bar')
plt.subplots_adjust(left=0.12, right=0.9, top=0.85, bottom=0.25)
plt.rcParams['animation.ffmpeg_path'] = '/Users/pouye/anaconda/bin/ffmpeg'
anim = animation.FuncAnimation(fig, cfs_anim_2Dkm_V,fargs=[arguments],frames=11,repeat=False)
anim.save('%s/%s-animation-mu%.1f-rake%.1f-V.mp4'%(CFSfilespath,CFSname,mu,rake))