#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  3 20:30:11 2020
by Pouye Yazdi
"""

import os
import sys
sys.path.append('Functions')
import numpy as np

# ===========================================================================
# ...... For plotting CFS result on horizontal cross sections
# ===========================================================================
from variables import CFSfilepath
from cfsPlotters import cfs_plot_2Dkm_H

CFSfilename='101-33--90/out-101-33-3AnnualINTERSSEsmoothm0p8-HorZ9km.txt'
earthquakefile=np.nan
percentage=10
mu=0.4
rake=-90
linenumber=4059+29
max_Stress=1#bar
cfs_plot_2Dkm_H(CFSfilename,CFSfilepath,mu,rake,linenumber,max_Stress,earthquakefile)
figfilename='%s_mu%.1f.png'%(CFSfilename[:-4],mu)

